<?php
	namespace Imran\BITM\SEIP106357\Utility;
	
	class Utility{
		
		static public function d($peram = false){
			echo "<pre>";
			var_dump($peram);
			echo "</pre>";
		}
		
		static public function dd($peram = false){
			self::d($peram);
			die();
		}
		
		static public function redirect($url="/bitm/AtomicProjectImran/views/SEIP106357/BookTitle"){
			header("Location:".$url);
		}
	}
?>