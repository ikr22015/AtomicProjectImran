<!DOCTYPE html>
<header>
	<title></title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device=width, initial-scale=1.0"/>
	<style>
		#utility{
			float:right;
			width:78%;
		}
	</style>
</header>
<body>
	<h1>Boot Title</h1>
	<div><span>Search/Filter</span><span id="utility">Download as PDF | XL | All New</span></div>
	
	<table border="1">
		<thead>
			<tr>
				<th>SL</th>
				<th>Book Title &dArr;</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td><a href="">PHP Code</a></td>
				<td>View | Edit | Delete | Tresh/Recover | Email to Friend</td>
			</tr>
			<tr>
				<td>2</td>
				<td><a href="">Clean Code Learning</a></td>
				<td>View | Edit | Delete | Tresh/Recover | Email to Friend</td>
			</tr>
		</tbody>
	
	</table>
	
</body>