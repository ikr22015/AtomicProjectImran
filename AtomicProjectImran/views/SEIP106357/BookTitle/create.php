<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
	<link rel="stylesheet" href="../../../resource/css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
	<link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Book Title</a></h1>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="addbook-sub-container">
				<h2 style="text-align:center">
					<?php
						include_once('../../../vendor/autoload.php');
						
						use Imran\BITM\SEIP106357\booktitle\Text;
						
						$book = new Text();
						
						echo $book->create();

					?>
				</h2>
				<form action="store.php" method="post">
					<fieldset>
						<label for="text">Enter Your Book Name</label>
						<input id="text"
								type="text"
								name="text" 
								value=""
								autofocus = "autofocus"
								placeholder="Book Name"
								required="required""
								/>
						<button type="submit" name="button">Submit</button>
						<input type="reset" name="" value="Reset"/>
					</fieldset>
				</form>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
