<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Subscription</title>
	<link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Subscription</a></h1>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="profile-sum-org-sub-container">
				<div id="profile-sum-org-panel-success" class="panel panel-success">
					<div id="profile-sum-org-panel-heading" class="panel-heading">Subscriber List</div>
					<form action="" method="">
						<table class="table table-bordered" border="1">
						  <tr>
							<th style="text-align:center">ID</th>
							<th style="text-align:center">Subscriber</th> 
							<th style="text-align:center">Action</th>
						  </tr>
						  <tr>
							<td>1</td>
							<td style="text-align:left">imran_kabir@hotmail.com</td> 
							<td>
								<button type="button" name="button" value=""><a href="view.php">View</a></button>
								<button type="button" name="button" value=""><a href="update.php">Edit</a></button>
								<button type="button" name="button" value=""><a href="delete.php">Delete</a></button>
							</td>
						  </tr>
						  <tr>
							<td>2</td>
							<td style="text-align:left">imran_kabir@hotmail.com</td> 
							<td>
								<button type="button" name="button" value=""><a href="view.php">View</a></button>
								<button type="button" name="button" value=""><a href="update.php">Edit</a></button>
								<button type="button" name="button" value=""><a href="delete.php">Delete</a></button>
							</td>
						  </tr>
						  <tr>
							<td>3</td>
							<td style="text-align:left">imran_kabir@hotmail.com</td> 
							<td>
								<button type="button" name="button" value=""><a href="view.php">View</a></button>
								<button type="button" name="button" value=""><a href="update.php">Edit</a></button>
								<button type="button" name="button" value=""><a href="delete.php">Delete</a></button>
							</td>
						  </tr>
						  <tr>
							<td>4</td>
							<td style="text-align:left">imran_kabir@hotmail.com</td> 
							<td>
								<button type="button" name="button" value=""><a href="view.php">View</a></button>
								<button type="button" name="button" value=""><a href="update.php">Edit</a></button>
								<button type="button" name="button" value=""><a href="delete.php">Delete</a></button>
							</td>
						  </tr>
						  <tr>
							<td>5</td>
							<td style="text-align:left">imran_kabir@hotmail.com</td> 
							<td>
								<button type="button" name="button" value=""><a href="view.php">View</a></button>
								<button type="button" name="button" value=""><a href="update.php">Edit</a></button>
								<button type="button" name="button" value=""><a href="delete.php">Delete</a></button>
							</td>
						  </tr>
						  <tr>
							<td>6</td>
							<td style="text-align:left">imran_kabir@hotmail.com</td> 
							<td>
								<button type="button" name="button" value=""><a href="view.php">View</a></button>
								<button type="button" name="button" value=""><a href="update.php">Edit</a></button>
								<button type="button" name="button" value=""><a href="delete.php">Delete</a></button>
							</td>
						  </tr>
						  <tr>
							<td>7</td>
							<td style="text-align:left">imran_kabir@hotmail.com</td> 
							<td>
								<button type="button" name="button" value=""><a href="view.php">View</a></button>
								<button type="button" name="button" value=""><a href="update.php">Edit</a></button>
								<button type="button" name="button" value=""><a href="delete.php">Delete</a></button>
							</td>
						  </tr>
						  <tr>
							<td>8</td>
							<td style="text-align:left">imran_kabir@hotmail.com</td> 
							<td>
								<button type="button" name="button" value=""><a href="view.php">View</a></button>
								<button type="button" name="button" value=""><a href="update.php">Edit</a></button>
								<button type="button" name="button" value=""><a href="delete.php">Delete</a></button>
							</td>
						  </tr>
						  <tr>
							<td>9</td>
							<td style="text-align:left">imran_kabir@hotmail.com</td> 
							<td>
								<button type="button" name="button" value=""><a href="view.php">View</a></button>
								<button type="button" name="button" value=""><a href="update.php">Edit</a></button>
								<button type="button" name="button" value=""><a href="delete.php">Delete</a></button>
							</td>
						  </tr>
						  <tr>
							<td>10</td>
							<td style="text-align:left">imran_kabir@hotmail.com</td> 
							<td>
								<button type="button" name="button" value=""><a href="view.php">View</a></button>
								<button type="button" name="button" value=""><a href="update.php">Edit</a></button>
								<button type="button" name="button" value=""><a href="delete.php">Delete</a></button>
							</td>
						  </tr>
						</table>
					</form>
				</div>
				<div class="list-sum-org-pagination">
					<nav>
					  <ul class="pagination">
						<li>
						  <a href="#" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						  </a>
						</li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li>
						  <a href="#" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						  </a>
						</li>
					  </ul>
					</nav>
				</div>
				<button type="button" name="create" value=""><a href="create.php">Create</a></button>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
