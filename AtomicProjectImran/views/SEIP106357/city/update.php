<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags --><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>City</title>
	<link rel="stylesheet" href="../../../resource/css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">City</a></h1>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="add-gender-sub-container">
				<h2>Update Your City</h2>
				<div class="form-group">
					<form action="#" method="post">
						<label>Enter Your Name</label>
						<input style="padding-left:5px;margin-left:7px" type="text" name="name" placeholder="Name" value="" /></br>
						<label for="sell">Which city do you live in?</label></br>
						<select class="form-control" id="sel1">
							<option>Dhaka</option>
							<option>Sylhet</option>
							<option>Rajshahi</option>
							<option>Chittagong</option>
							<option>Khulna</option>
							<option>Borishal</option>
							<option>Joshor</option>
						</select>
						<button style="margin-top:10px" type="submit" name="button" value="">Submit</button>
					</form>
				</div>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

    <title>Hobby</title>
	<link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.html">Hobby</a></h1>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="add-gender-sub-container">
				<h2>Update Hobby</h2>
				<form action="#" method="post">
						<label>Enter Your Name</label>
						<input style="padding-left:5px;margin-left:7px" type="text" name="name" placeholder="Name" value="" /></br>
						<label>Choose your hobbies.</label></br>
						<table class="table table-condensed">
							<tr class="active">
								<td><input type="checkbox" name="football" value=""/>Football</td>
								<td><input type="checkbox" name="chees" value=""/>Chees</td>		
								<td><input type="checkbox" name="cricket" value=""/>Cricket</td>
							</tr>
							<tr class="success">
								<td><input type="checkbox" name="football" value=""/>Watching TV</td>
								<td><input type="checkbox" name="chees" value=""/>Family Time</td>		
								<td><input type="checkbox" name="cricket" value=""/>Fishing</td>
							</tr>
							<tr class="active">
								<td><input type="checkbox" name="football" value=""/>Computer</td>
								<td><input type="checkbox" name="chees" value=""/>Listening to Music</td>		
								<td><input type="checkbox" name="cricket" value=""/>Hunting</td>
							</tr>
							<tr class="success">
								<td><input type="checkbox" name="football" value=""/>Shopping</td>
								<td><input type="checkbox" name="chees" value=""/>Traveling</td>		
								<td><input type="checkbox" name="cricket" value=""/>Sleeping</td>
							</tr>
						</table>
						<button type="submit" name="button" value="">Submit</button>
				</form>
				<button style="float:right" type="button" name="button" value=""><a href="hobby-list.html">Hobby List</a></button>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
